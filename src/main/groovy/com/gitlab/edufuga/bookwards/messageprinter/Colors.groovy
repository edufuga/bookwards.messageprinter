package com.gitlab.edufuga.bookwards.messageprinter

import groovy.transform.CompileStatic

import java.awt.Color

@CompileStatic
class Colors {
    static Color noopt = new Color(255, 255, 255)
    static Color ignored = new Color(255, 153, 204)
    static Color unknown = new Color(255, 99, 71)
    static Color guessed = new Color(255, 153, 51)
    static Color comprehended = new Color(178, 102, 255)
    static Color known = new Color(102, 178, 255)
    static Color wellKnown = new Color(51, 153, 255)
    static Color mastered = new Color(51, 153, 255)
}
