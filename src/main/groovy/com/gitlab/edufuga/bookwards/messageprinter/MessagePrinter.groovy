package com.gitlab.edufuga.bookwards.messageprinter

import com.gitlab.edufuga.wordlines.core.Status
import com.gitlab.edufuga.wordlines.core.WordStatusDate
import groovy.transform.CompileStatic
import org.fusesource.jansi.Ansi

import java.awt.Color
import java.util.function.Supplier

import static org.fusesource.jansi.Ansi.ansi

@CompileStatic
class MessagePrinter
{
   void printMessage(String message, Color textColor = Colors.noopt, boolean eol = true,
                     String addition = "")
           throws IOException {
      if (message == null || message.isEmpty()) {
         return
      }
      Ansi ansi = ansi().fgRgb(textColor.getRGB()).a(message)
      if (addition) {
         ansi.a(" ($addition)")
      }
      if (eol) {
         ansi.a("\n")
      }

      System.out.print(ansi.reset().toString())
   }

   void printMessage(String message, Map<Integer, Color> characterPositionToColour,
                     Map<Integer, Boolean> charactersToUnderline,
                     boolean eol = false, boolean hide = false) {
      if (message == null || message.isEmpty()) {
         return
      }

      boolean underline = !(charactersToUnderline == null || charactersToUnderline.isEmpty())

      int idx = 0
      for (char character : (message as List<Character>))
      {
         try {
            Color textColor = characterPositionToColour.getOrDefault(idx, Colors.noopt)

            if (underline) {
               boolean underlineCharacter = charactersToUnderline.getOrDefault(idx, false)
               if (underlineCharacter) {
                  // Assumption: The characters to underline are the same as the characters to hide (if enabled).
                  if (hide) {
                     character = ' ' as char
                  }
                  print ansi().fgRgb(textColor.getRGB())
                          .a(Ansi.Attribute.UNDERLINE)
                          .a(character)
                          .a(Ansi.Attribute.UNDERLINE_OFF)
               }
               else {
                  print ansi().fgRgb(textColor.getRGB()).a(character)
               }
            }
            else {
               print ansi().fgRgb(textColor.getRGB()).a(character)
            }
         }
         catch (Exception e) {
            e.printStackTrace()
         }
         idx++
      }
      if (eol) {
         print ansi().a("\n")
      }
   }

   void printMessage(String message, List<WordStatusDate> words,
                     Supplier<Map<Integer, Boolean>> charactersToUnderlineSupplier,
                     boolean eol,
                     boolean color=true, boolean hide=false) {
      // Use indexOf for finding the position within the string? The length is the length of the word.
      Map<Integer, Color> characterPositionToColour = new HashMap<>()

      // Underline asked word
      Map<Integer, Boolean> charactersToUnderline = charactersToUnderlineSupplier.get ()

      if (color) {
         // Find the right colour for every word and their characters.
         int indexInMapping = 0 // This corresponds to the index within the message string (0 to message.length-1).
         for (WordStatusDate word : words) {
            int indexOfWordStart = message.indexOf(word.word, indexInMapping)
            int indexOfWordEnd = indexOfWordStart + word.word.size()
            indexInMapping = indexOfWordStart

            // Find colouring for word
            Color textColor
            switch (word.status) {
               case null: textColor = Colors.noopt; break
               case Status.IGNORED: textColor = Colors.ignored; break
               case Status.UNKNOWN: textColor = Colors.unknown; break
               case Status.GUESSED: textColor = Colors.guessed; break
               case Status.COMPREHENDED: textColor = Colors.comprehended; break
               case Status.KNOWN: textColor = Colors.known; break
               case Status.WELL_KNOWN: textColor = Colors.wellKnown; break
               case Status.MASTERED: textColor = Colors.mastered; break
               default: textColor = Colors.noopt; break
            }

            // Fill the mapping of the colour that corresponds to each word (a word is a collection of characters)
            // Each character of the same word has the same colour.
            for (char character : word.word.chars) {
               characterPositionToColour.put(indexInMapping, textColor)
               indexInMapping++ // increase the count within each word (corresponds to the characters in a word)
            }
         }
      }

      // Use characterPositionToColour to print each character accordingly.
      printMessage(message, characterPositionToColour, charactersToUnderline, eol, hide)
   }

   // This is not really part of the MessagePrinter, but well.
   static Map<Integer, Boolean> getCharactersToUnderline(String message, String askedWord) {
      Map<Integer, Boolean> charactersToUnderline = new HashMap<>()
      int indexOfAskedWordWithinMessage = message.indexOf(askedWord)
      // TODO: Exclude false matches like "mich" for the word "ich".

      if (indexOfAskedWordWithinMessage == -1)
      {
         return Collections.emptyMap ()
      }

      for (int idx in (indexOfAskedWordWithinMessage..<(indexOfAskedWordWithinMessage + askedWord.size()))) {
         charactersToUnderline.put(idx, true)
      }

      return charactersToUnderline
   }

   static void main(String[] args) {
      MessagePrinter messagePrinter = new MessagePrinter()
      messagePrinter.printMessage("This is a message", Colors.comprehended)
      messagePrinter.printMessage("This is another message", Colors.known)

      messagePrinter.printMessage("Underline me", [:], [0:true, 1:true, 2:true, 3:true, 4:true])
   }
}